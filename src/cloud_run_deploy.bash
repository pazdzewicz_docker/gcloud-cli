## utils

error_and_exit() {
  local MESSAGE="${1}"
  local HINT="${2}"
  echo ""
  echo "🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥"
  echo ""
  echo "Error \`cloud_run_deploy.bash\`"
  echo "===================="
  echo ""
  echo "Message"
  echo "-------"
  echo "${MESSAGE}"
  echo ""
  echo "Hint"
  echo "----"
  echo "${HINT}"
  echo ""
  echo "/end"
  echo ""
  exit 1
}

## required environment variables

if [[ -z "${GCP_PROJECT_ID}" ]]; then
  error_and_exit "\${GCP_PROJECT_ID} is not set" "Did you setup a service account?"
fi

if [[ -z "${GCP_SERVICE_ACCOUNT}" ]]; then
  error_and_exit "\${GCP_SERVICE_ACCOUNT} is not set" "Did you setup a service account?"
fi

if [[ -z "${GCP_SERVICE_ACCOUNT_KEY}" ]]; then
  error_and_exit "\${GCP_SERVICE_ACCOUNT_KEY} is not set" "Did you setup a service account?"
fi

if [[ -z "${CI_PROJECT_ID}" ]]; then
  error_and_exit "\${CI_PROJECT_ID} is not set"
fi

if [[ -z "${CI_COMMIT_REF_SLUG}" ]]; then
  error_and_exit "\${CI_COMMIT_REF_SLUG} is not set"
fi

if [[ -z "${CONTAINER_IMAGE}" ]]; then
  error_and_exit "\${CONTAINER_IMAGE} is not set"
fi


## optional environment variables

GCP_REGION="${GCP_REGION:-us-central1}"
GCP_CLOUD_RUN_MAX_INSTANCES="${GCP_CLOUD_RUN_MAX_INSTANCES:-100}"
GCP_CLOUD_RUN_MIN_INSTANCES="${GCP_CLOUD_RUN_MIN_INSTANCES:-1}"
GCP_CLOUD_RUN_TIMEOUT="${GCP_CLOUD_RUN_TIMEOUT:-300}"
GCP_CLOUD_RUN_CONCURRENCY="${GCP_CLOUD_RUN_CONCURRENCY:-80}"
GCP_CLOUD_RUN_MEMORY="${GCP_CLOUD_RUN_MEMORY:-512Mi}"
GCP_CLOUD_RUN_CPU="${GCP_CLOUD_RUN_CPU:-1000m}"
GCP_CONTAINER_PORT="${GCP_CONTAINER_PORT:-8080}"

## private variables

SERVICE_NAME="gitlab-${CI_PROJECT_ID}-${CI_COMMIT_REF_SLUG}"
__GCP_SERVICE_ACCOUNT_KEY_PRIVATE_KEY_DATA_FILE_NAME=local-service-account-key-private-key-data.txt
__GCP_SERVICE_ACCOUNT_KEY_FILE_NAME=local-service-account-key-file.json

## environment variables

ENV_VAR_STRING=""
ENV_VARIABLES=""

GCP_ENV_MYSQL_DATABASE="${GCP_ENV_MYSQL_DATABASE:GCP_CLOUDSQL_DATABASE_NAME}"
GCP_ENV_MYSQL_HOST="${GCP_ENV_MYSQL_HOST:GCP_CLOUDSQL_PRIMARY_IP_ADDRESS}"
GCP_ENV_MYSQL_USER="${GCP_ENV_MYSQL_USER:GCP_CLOUDSQL_DATABASE_USER}"
GCP_ENV_MYSQL_PASSWORD="${GCP_ENV_MYSQL_PASSWORD:GCP_CLOUDSQL_DATABASE_PASS}"

for ENV_VAR in $(printenv | grep GCP_ENV_)
do
    ENV_VARIABLES="${ENV_VARIABLES} $(echo "${ENV_VAR}" | sed 's/GCP_ENV_//g')"
done

ENV_VAR_STRING="$(echo ${ENV_VARIABLES} | sed 's/ /, /g')"

## secret variables

SECRET_VAR_STRING=""
SECRET_VARIABLES=""

for SECRET_VAR in $(printenv | grep GCP_SECRET_)
do
    SECRET_VARIABLES="${SECRET_VARIABLES} $(echo "${SECRET_VAR}" | sed 's/GCP_SECRET_//g')"
done

SECRET_VAR_STRING="$(echo ${SECRET_VARIABLES} | sed 's/ /, /g')"

echo ${SECRET_VAR_STRING}

## cleanup tmp files

rm --force ${__GCP_SERVICE_ACCOUNT_KEY_PRIVATE_KEY_DATA_FILE_NAME}
rm --force ${__GCP_SERVICE_ACCOUNT_KEY_FILE_NAME}
rm --force deploy.env
rm --force local-output.log

## extract service account key file

case "${GCP_SERVICE_ACCOUNT_KEY}" in
  *privateKeyData*)
    echo "🟩🟩 '.privateKeyData' found"
    echo "${GCP_SERVICE_ACCOUNT_KEY}" | jq --raw-output '.privateKeyData' > ${__GCP_SERVICE_ACCOUNT_KEY_PRIVATE_KEY_DATA_FILE_NAME}
    base64 --decode ${__GCP_SERVICE_ACCOUNT_KEY_PRIVATE_KEY_DATA_FILE_NAME} > ${__GCP_SERVICE_ACCOUNT_KEY_FILE_NAME}
    ;;
  *private_key_data*)
    echo "🟩🟩 '.private_key_data' found"
    echo "${GCP_SERVICE_ACCOUNT_KEY}" | jq --raw-output '.private_key_data' > ${__GCP_SERVICE_ACCOUNT_KEY_FILE_NAME}
    ;;
  *)
    error_and_exit "Failed to extract service account key file" "Did you setup a service account?"
    ;;
esac

## gcloud auth and configure

gcloud auth activate-service-account --key-file ${__GCP_SERVICE_ACCOUNT_KEY_FILE_NAME} || error_and_exit "Failed to activate service account"
gcloud config set project "${GCP_PROJECT_ID}" || error_and_exit "Failed to set GCP project"

## gcloud run deploy

gcloud run deploy "${SERVICE_NAME}" --quiet \
    --image "${CONTAINER_IMAGE}" \
    --port "${GCP_CONTAINER_PORT}"\
    --region="${GCP_REGION}" \
    --max-instances="${GCP_CLOUD_RUN_MAX_INSTANCES}" \
    --min-instances="${GCP_CLOUD_RUN_MIN_INSTANCES}" \
    --timeout="${GCP_CLOUD_RUN_TIMEOUT}" \
    --concurrency="${GCP_CLOUD_RUN_CONCURRENCY}" \
    --memory="${GCP_CLOUD_RUN_MEMORY}" \
    --cpu="${GCP_CLOUD_RUN_CPU}" \
    --allow-unauthenticated \
    --update-env-vars "${ENV_VAR_STRING}" \
    --update-secrets "${SECRET_VAR_STRING}" \
    --format 'value(status.url)' >> local-output.log || error_and_exit "Failed to deploy service"

## generate deploy.env

echo "DYNAMIC_URL=$(cat local-output.log)" >>deploy.env || error_and_exit "Failed to decode service account key"