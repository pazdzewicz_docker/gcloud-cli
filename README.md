# gcloud-cli

This Image contains gcloud-cli and a few tools

## Parent image

- `google/cloud-sdk:latest`

## Dependencies

None

## Entrypoint & CMD

Since this Image is only used during CI we don't need an entrypoint.

## Functions

- `/ci_tools/cloud_run_deploy.bash` - Fork of [cloud-run.sh](https://gitlab.com/gitlab-org/incubation-engineering/five-minute-production/library/-/raw/main/gcp/cloud-run.sh)

## Build Image

### Automated Build

Through the new build pipeline all of our Docker images are built via Gitlab CI/CD and pushed to the Gitlab Container Registry. You usually don't need to build the image on your own, just push your commits to the git. The Image will be tagged after your branch name.

After the automated build is finished you can pull the image:

```
docker pull registry.gitlab.com/pazdzewicz_docker/gcloud-cli:BRANCH
```

All images also have a master tag, just replace the BRANCH with "master". The "latest" Tag is only an alias for the "master" tag.

### Manual Build

You can also build the Docker image on your local pc:

```
docker build -t "mygcloud-cli:latest" .
```

### Build Options

Build Options are Arguments in the Dockerfile (`--build-arg`):

- None

### Security Check during Build

Before we release Docker Containers into the wild it is required to run the Anchore security checks over the Pipeline (this doesn't apply to manual built images). You can download the current artifacts on the Pipelines page (the download drop-down).

## Environment Variables:

### `/ci_tools/cloud_run_deploy.bash`:

- `CONTAINER_IMAGE` - Docker Image to deploya
- `GCP_CONTAINER_PORT` - Docker Port to deploy
- `CI_PROJECT_ID` - Gitlab Project ID
- `CI_COMMIT_REF_SLUG` - Gitlab Commit Ref Slug
- `GCP_PROJECT_ID` - Google Cloud Project ID
- `GCP_SERVICE_ACCOUNT` - Google Cloud Service Account
- `GCP_SERVICE_ACCOUNT_KEY` - Google Cloud Service Account Key
- `GCP_REGION` - Google Cloud Region
- `GCP_CLOUD_RUN_MAX_INSTANCES` - Google Cloud Run Max Instances
- `GCP_CLOUD_RUN_MIN_INSTANCES` - Google Cloud Run Min Instances
- `GCP_CLOUD_RUN_TIMEOUT` - Google Cloud Run Timeout
- `GCP_CLOUD_RUN_CONCURRENCY` - Google Cloud Run Concurrency
- `GCP_CLOUD_RUN_MEMORY` - Google Cloud Run Memory
- `GCP_CLOUD_RUN_CPU` - Google Cloud Run CPU
- `GCP_ENV_YOUR_CUSTOM_VARIABLE` - See "How to use env variables"
- `GCP_SECRET_YOUR_CUSTOM_VARIABLE` - See "How to use env variables"

#### How to use env variables

Add `GCP_ENV_` as prefix to your custom environment

Example:

```
GCP_ENV_APPLICATION_URL="https://my.application.com/"
```

#### How to use secrets

Add secret to secret manager and then you can use `GCP_SECRET_` as prefix for your secret and use it in your environment

Example:

```
GCP_SECRET_MYSQL_PASSWORD="MYSQL_PASSWORD:latest"
```

### `/ci_tools/cloud_run_undeploy.bash`:

- `CI_PROJECT_ID` - Gitlab Project ID
- `CI_COMMIT_REF_SLUG` - Gitlab Commit Ref Slug
- `GCP_PROJECT_ID` - Google Cloud Project ID
- `GCP_SERVICE_ACCOUNT` - Google Cloud Service Account
- `GCP_SERVICE_ACCOUNT_KEY` - Google Cloud Service Account Key
- `GCP_REGION` - Google Cloud Region

## Ports

- None

## Usage

### Deploy to Google Cloud Run

Please set above mentioned environment variables

```
####
# mirror to google cloud
####

application_mirror_to_google_cloud:
  stage: build_latest
  services:
    - docker:dind
  variables:
    MIRROR_IMAGE: ${GOOGLE_IMAGE}/app
    CONTAINER_IMAGE: ${CI_REGISTRY_IMAGE}/app
  script:
    - docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
    - echo ${GOOGLE_ACCOUNT} > account.json
    - docker login -u ${GOOGLE_REGISTRY_USER} --password-stdin ${GOOGLE_REGISTRY} < account.json
    - docker pull ${CONTAINER_IMAGE}:${CI_COMMIT_SHA}
    - docker tag ${CONTAINER_IMAGE}:${CI_COMMIT_SHA} ${MIRROR_IMAGE}:${CI_COMMIT_SHA}
    - docker push ${MIRROR_IMAGE}:${CI_COMMIT_SHA}
    - docker tag ${CONTAINER_IMAGE}:${CI_COMMIT_SHA} ${MIRROR_IMAGE}:master
    - docker push ${MIRROR_IMAGE}:master
    - docker tag ${CONTAINER_IMAGE}:${CI_COMMIT_SHA} ${MIRROR_IMAGE}:latest
    - docker push ${MIRROR_IMAGE}:latest
  only:
    - master
    - main
  needs:
    - application_build_and_push

#####
# Deploy to Google Cloud Run
#####

application_deploy_to_google_cloud:
  image: registry.gitlab.com/pazdzewicz_docker/gcloud-cli:master
  stage: deploy
  variables:
    CONTAINER_IMAGE: ${GOOGLE_IMAGE}/app:${CI_COMMIT_SHA}
  script:
    - /ci_tools/cloud_run.bash
  only:
    - master
    - main
  environment:
    name: $CI_COMMIT_REF_NAME
    url: $DYNAMIC_URL
  needs:
    - application_mirror_to_google_cloud
  artifacts:
    reports:
      dotenv: deploy.env
```

### Un-Deploy to Google Cloud Run

```
####
# undeploy to google cloud
####

application_undeploy_to_google_cloud:
  image: registry.gitlab.com/pazdzewicz_docker/gcloud-cli:master
  stage: deploy
  when: manual
  environment:
    name: $CI_COMMIT_REF_NAME
    action: stop
  script:
    - /ci_tools/cloud_run_undeploy.bash
  only:
    - master
    - main
  needs:
    - application_deploy_to_google_cloud
```