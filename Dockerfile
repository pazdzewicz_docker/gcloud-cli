FROM google/cloud-sdk:debian_component_based

RUN apt-get update && \
    apt-get install -y wget curl jq && \
    mkdir -p /ci_tools/

COPY src/ /ci_tools/

RUN for FILE in $(find /ci_tools/ -iname "*.bash"); do chmod +x "${FILE}"; done